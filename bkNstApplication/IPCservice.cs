﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.AppService;
using Windows.Foundation;
using Windows.Foundation.Collections;

namespace bkNstApplication
{
    class IPCservice
    {


        private static AppServiceConnection _service;
        const string AppServiceName = "bkIPCAppService";

        public static async Task InitAsync()
        {
            _service = await MakeConnection();
            var serviceStatus = await _service.OpenAsync();
            // Should never fail, since app service is installed with the background app.
            Debug.Assert(serviceStatus == AppServiceConnectionStatus.Success, $"Opening service failed: {serviceStatus}.");
            //_service.RequestReceived += ConnectionOnRequestReceived;

            _service.RequestReceived += async (AppServiceConnection sender, AppServiceRequestReceivedEventArgs args) => await ConnectionOnRequestReceived(sender, args);


            _service.ServiceClosed += async (AppServiceConnection sender, AppServiceClosedEventArgs args) =>
            {
                _service = null;
                Debug.WriteLine($"Service closed: {args.Status}.");
                await InitAsync();
            };
            Debug.WriteLine("Connected to app service.");

        }

        private async static Task<AppServiceConnection> MakeConnection()
        {

            var listing = await AppServiceCatalog.FindAppServiceProvidersAsync(AppServiceName);

            if (listing.Count == 0)
            {
                throw new Exception("Unable to find app service '" + AppServiceName + "'");

            }
            var packageName = listing[0].PackageFamilyName;

            var connection = new AppServiceConnection
            {
                AppServiceName = AppServiceName,
                PackageFamilyName = packageName
            };

            return connection;

        }



        public static async Task SendMessageAsync(ValueSet message)
        {
            try
            {
                var task = _service?.SendMessageAsync(message);
                if (task != null)
                {
                    await task;
                }
                else
                {
                    Debug.WriteLine("Skipping message: App service connection is null.");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Sending message failed: {e.Message}.");
            }
        }

       


        private static async Task ConnectionOnRequestReceived(AppServiceConnection sender, AppServiceRequestReceivedEventArgs args)
        {
            var appServiceDeferral = args.GetDeferral();
            try
            {

                object message;
                object send;
                args.Request.Message.TryGetValue("Message", out message);
                args.Request.Message.TryGetValue("Sender", out send);
                Debug.WriteLine("++++ bkNstApplication - Receive: " + message.ToString());

                if (send.ToString().Equals("bkPortManager"))
                {
                    await NstApp.convertDataToNstMessage(message.ToString());//metatrepoyme to object se string giati exoyme value set
                }
                else if (send.ToString().Equals("bkCloudManager"))
                {
                    await NstApp.processCommandReceived(message.ToString());
                }
                else
                    Debug.WriteLine("!!!!! bkNSTApplication - Message with unkonwn sender received");

            }
            finally
            {
                appServiceDeferral.Complete();
            }
        }
        //public static TypedEventHandler<AppServiceConnection, AppServiceRequestReceivedEventArgs> RequestReceived;
    }
}


