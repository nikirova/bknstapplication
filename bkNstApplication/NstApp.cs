﻿using Microsoft.Data.Sqlite;
using comNST;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace bkNstApplication
{
    class NstApp
    {

        // USED ONLY FOR HBE commands
        // for each valid CMG, the dictionary contains: CMG ipnode and the corresponding HBE token
        public static Dictionary<string, string> cmgHBETokens = new Dictionary<string, string>();
        public static string[] CMGsDeclaredinDB = new string[8];
        public static int numOfCMGsDeclaredinDB = 0;

        //Enable shared cache from database
        [DllImport("sqlite3.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int sqlite3_enable_shared_cache(int enable);

        /// <summary>
        /// mpla mapl
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>


        //-------------------- Receives data from Port Manager and convert them to NstMessage--------------------//
        public async static Task convertDataToNstMessage(string input)
        {
            try
            {
                int maxMessageLength = 35;
                if ((input.Length % maxMessageLength) != 0)
                {
                    Debug.WriteLine("The number " + input.Length.ToString() + " of bytes read wasn't multiple of 10");
                }
                else
                {
                    int len = input.Length;
                    int last = input.LastIndexOf('\n'); //index on the last occurance of \n
                    if (last != len - 1)
                    {
                        Debug.WriteLine(input + " : the last byte is not '\n'! Some bytes will be discarded");
                        await Logging.WriteDebugLog("{0} The last byte is not '\n'! Some bytes will be discarded", input);
                    }
                    string newinput = input.Substring(last - last);
                    char[] delimiterChars = { '\n' };
                    string[] messages = newinput.Split(delimiterChars);

                    bool exitFlag = false;

                    foreach (string inputMessage in messages)
                    {
                        if (inputMessage != "")
                        {
                            //check whether the input string is in correct format
                            exitFlag = false;
                            if (inputMessage.Length != 34)
                            {
                                Debug.WriteLine(inputMessage + " : Wrong length of input data: " + inputMessage.Length.ToString() + ".Message discarded");
                                await Logging.WriteDebugLog("{0} Wrong length of input data, message will be discarded", input);
                                exitFlag = true;
                            }
                            else
                            {
                                comNST.NstMessage nstmessage = new comNST.NstMessage();

                                //Reads RESPONSE_CODE string and add it to nstMessage structure
                                string responseCode = inputMessage.Substring(0, 2); // position 0, 2 digits
                                nstmessage.Response = responseCode;
                                Debug.WriteLine(" Response Code is: " + responseCode);

                                //Reads TOKEN string and add it to nstMessage structure 
                                string token = inputMessage.Substring(2, 16); //position 2, 16 digits
                                nstmessage.Token = token;
                                Debug.WriteLine(" Token is: " + token);
                                exitFlag = false;

                                //Reads TOKEN_LENGTH string and add it to nstMessage structure 
                                string tokenlength = inputMessage.Substring(18, 2); //position 18, 2 digits
                                nstmessage.Tokenlength = tokenlength;
                                Debug.WriteLine(" Token length is: " + tokenlength);
                                exitFlag = false;

                                //Reads IPNODE string and add it to nstMessage structure
                                string ipnode = inputMessage.Substring(20, 4); //position 20, 4 digits
                                nstmessage.Ipnode = ipnode;
                                Debug.WriteLine(" IPnode is: " + ipnode);
                                exitFlag = false;

                                //Reads RESOURCE string, checks whether the string value contains "--", if not add it to nstMessage else ignore it
                                string resource = inputMessage.Substring(24, 2);
                                if (!resource.Equals("--"))
                                {
                                    nstmessage.Resource = resource;
                                    Debug.WriteLine("Resource is: " + resource);
                                }
                                exitFlag = false;

                                //Reads VALUE (integer/decimal) string, checks whether the string value contains "--", if not add it to nstMessage else ignore it

                                string integerValue = inputMessage.Substring(27, 5); //position 27, 5 digits
                                string decimalValue = inputMessage.Substring(32, 2); //position 32, 2 digits

                                if (!integerValue.Equals("-----"))
                                {
                                    string fullStringValue = integerValue + "." + decimalValue;
                                    //checks if VALUE contains only numbers, / d = [0 - 9]
                                    bool containsNum = Regex.IsMatch(fullStringValue, @"\d");

                                    if (containsNum)
                                    {
                                        CultureInfo culture = new CultureInfo("en-US");
                                        decimal value = 0;

                                        if (inputMessage[26] == '+')
                                        {
                                            value = Convert.ToDecimal(fullStringValue, culture);
                                            nstmessage.Value = (double)value;
                                        }

                                        else if (inputMessage[26] == '-')
                                        {
                                            value = 0 - Convert.ToDecimal(fullStringValue, culture);
                                            nstmessage.Value = (double)value;
                                        }
                                        else
                                        {
                                            Debug.WriteLine("The sign is invalid: " + inputMessage[26]);
                                            exitFlag = true;
                                            await Logging.WriteDebugLog("{0}: The sign is invalid: {1}", input, inputMessage[26].ToString());
                                        }

                                    }
                                    else
                                    {
                                        exitFlag = true;
                                        Debug.WriteLine("The VALUE field format is wrong" + nstmessage.Value);
                                        await Logging.WriteDebugLog("{0}: The VALUE field format {1} is wrong ", input, nstmessage.Value.ToString());
                                    }
                                }
                                if (exitFlag == false)
                                {
                                    await Logging.WriteDebugLog("The NstMessage is {0}", nstmessage.Stringify());
                                    await processNstMessage(nstmessage);
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("convertDataToNstMessage: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("convertDataToNstMessage {0} ", ex.Message);
            }
        }



        //-------------------------------- Process NstMessage-------------------------------------------//
        public async static Task processNstMessage(NstMessage nstmessage)
        {
            try
            {
                switch (nstmessage.Response)
                {
                    case NstMessage.TELE_RESPONSE:

                        string stringMessageTelemetry = nstmessage.Stringify();
                        Debug.WriteLine(stringMessageTelemetry + " : Send TELEMETRY DATA as Json message");
                        Debug.WriteLine(stringMessageTelemetry + "\n--------------------------");

                        await sendToCloudManager(stringMessageTelemetry);
                        break;

                    case NstMessage.ALERT_RESPONSE:

                        string stringMessageAlert = nstmessage.Stringify();
                        Debug.WriteLine(stringMessageAlert + " : Send ALERT as Json message");
                        Debug.WriteLine(stringMessageAlert + "\n--------------------------");

                        await sendToCloudManager(stringMessageAlert);

                        break;

                    case NstMessage.OK_RESPONSE:

                        // check whether it refers to a HBE command
                        string responseIpNode = nstmessage.Ipnode;
                        string responseToken = nstmessage.Token;
                        bool containsIpnode = cmgHBETokens.TryGetValue(responseIpNode, out string tokenSent);

                        if ((containsIpnode == true) && (tokenSent.Equals(responseToken.ToUpper())))
                        {
                            // REFERs to HBE command
                            await Logging.WriteDebugLog("OK response to HBE command with {0} token", nstmessage.Token);
                            cmgHBETokens.Remove(responseIpNode);
                            // TBD
                            // delete command from app_commands
                        }
                        else
                        {
                            bool tokenExistOK = await dbCheckTokenResponse(nstmessage.Token);

                            if (tokenExistOK == true) //checks if token exists in database
                            {
                                string stringMessageOkResponse = nstmessage.Stringify();
                                Debug.WriteLine(stringMessageOkResponse + " : Send OK RESPONSE as Json message");
                                Debug.WriteLine(stringMessageOkResponse + "\n--------------------------");

                                await sendToCloudManager(stringMessageOkResponse);
                            }
                            else
                            {
                                //Handle error
                                Debug.WriteLine("This token does not exists in database" + nstmessage.Token);
                                await Logging.WriteDebugLog("This token does not exists in database - {0}", nstmessage.Token);
                            }
                        }

                        break;

                    case NstMessage.NOK_RESPONSE:

                        bool tokenExistNOK = await dbCheckTokenResponse(nstmessage.Token);

                        if (tokenExistNOK == true) //checks if token exists in database
                        {
                            string stringMessageΝΟΚResponse = nstmessage.Stringify();
                            Debug.WriteLine(stringMessageΝΟΚResponse + " : Send ΝOK RESPONSE as Json message");
                            Debug.WriteLine(stringMessageΝΟΚResponse + "\n--------------------------");

                            await sendToCloudManager(stringMessageΝΟΚResponse);
                        }
                        else
                        {
                            //Handle error
                            Debug.WriteLine("This token does not exists in database" + nstmessage.Token);
                            await Logging.WriteDebugLog("This token does not exists in database - {0}", nstmessage.Token);
                        }

                        break;

                    case NstMessage.BUSY_RESPONSE:

                        // check whether it refers to a HBE command
                        responseIpNode = nstmessage.Ipnode;
                        responseToken = nstmessage.Token;
                        containsIpnode = cmgHBETokens.TryGetValue(responseIpNode, out tokenSent);
                        if ((containsIpnode == true) && (tokenSent.Equals(responseToken.ToUpper())))
                        {
                            // REFERs to HBE command
                            await Logging.WriteDebugLog("Busy response to HBE command with {0} token", nstmessage.Token);
                            cmgHBETokens.Remove(responseIpNode);
                            // TBD
                            // delete command from app_commands
                        }
                        else
                        {
                            bool tokenExistOK = await dbCheckTokenResponse(nstmessage.Token);
                            if (tokenExistOK == true) //checks if token exists in database
                            {
                                //future work: Send to cloud the CMG ipnode

                                //(Send message to user: CMG is busy, please try again later)
                                string stringMessageBUSYResponse = nstmessage.Stringify();
                                Debug.WriteLine(stringMessageBUSYResponse + " : Send BUSY RESPONSE as Json message");
                                Debug.WriteLine(stringMessageBUSYResponse + "\n--------------------------");

                                await sendToCloudManager(stringMessageBUSYResponse);
                            }
                            else
                            {
                                //Handle error
                                Debug.WriteLine("This token does not exists in database" + nstmessage.Token);
                                await Logging.WriteDebugLog("This token does not exists in database - {0}", nstmessage.Token);
                            }
                        }

                        break;

                    case NstMessage.RNF_RESPONSE:

                        //(Send message to user: Resource not found)

                        bool tokenExistRNF = await dbCheckTokenResponse(nstmessage.Token);

                        if (tokenExistRNF == true) //checks if token exists in database
                        {
                            string stringMessageRNFResponse = nstmessage.Stringify();
                            Debug.WriteLine(stringMessageRNFResponse + " : Send RNF RESPONSE as Json message");
                            Debug.WriteLine(stringMessageRNFResponse + "\n--------------------------");

                            await sendToCloudManager(stringMessageRNFResponse);
                        }
                        else
                        {
                            //Handle error
                            Debug.WriteLine("This token does not exists in database" + nstmessage.Token);
                            await Logging.WriteDebugLog("This token does not exists in database - {0}", nstmessage.Token);
                        }
                        break;

                    case NstMessage.SNF_RESPONSE: //Server not found - means CMD is not on network

                        bool tokenExistSNF = await dbCheckTokenResponse(nstmessage.Token);
                        if (tokenExistSNF == true) //checks if token exists in database
                        {
                            string stringMessageSNFResponse = nstmessage.Stringify();
                            Debug.WriteLine(stringMessageSNFResponse + " : Send SNF RESPONSE as Json message");
                            Debug.WriteLine(stringMessageSNFResponse + "\n--------------------------");

                            await sendToCloudManager(stringMessageSNFResponse);


                            //prepare and send a RESET command to CMG
                            //    for (int i = 0; i < NstApp.numOfCMGsDeclaredinDB; i++)
                            //    {
                            //        bool commandSupported = await NstApp.dbCheckSupportedCommand(NstMessage.RSG_COMMAND);

                            //        if (commandSupported == true)
                            //        {

                            //            NstMessage sendRSG = new NstMessage();

                            //            sendRSG.Command = NstMessage.RSG_COMMAND;
                            //            sendRSG.Ipnode = CMGsDeclaredinDB[i];
                            //            Debug.WriteLine("Send RSG command (RESET) to CMG[ipnode]: " + sendRSG.Ipnode);
                            //            await Logging.WriteDebugLog("Send RSG command (RESET) to CMG[ipnode] - {0}", sendRSG.Ipnode);

                            //            string token = await NstApp.dbAddReceivedCommand(sendRSG);
                            //            sendRSG.Token = token;

                            //            // send command to Port Manager
                            //            ValueSet sentData = new ValueSet();
                            //            sentData.Add("Token", sendRSG.Stringify());
                            //            sentData.Add("Sender", "NstApplication");
                            //            sentData.Add("Receiver", "bkPortManager");
                            //            await IPCservice.SendMessageAsync(sentData);


                            //        }
                            //    }
                        }
                        else
                        {
                            //Handle error
                            Debug.WriteLine("This token does not exists in database" + nstmessage.Token);
                            await Logging.WriteDebugLog("This token does not exists in database - {0}", nstmessage.Token);
                        }
                        break;

                    case NstMessage.WVDR_RESPONSE: //Wrong Value from Digital Resource

                        bool tokenExistWVDR = await dbCheckTokenResponse(nstmessage.Token);
                        if (tokenExistWVDR == true) //checks if token exists in database
                        {
                            string stringMessageWVDRResponse = nstmessage.Stringify();
                            Debug.WriteLine(stringMessageWVDRResponse + " : Send WVDR RESPONSE as Json message");
                            Debug.WriteLine(stringMessageWVDRResponse + "\n--------------------------");

                            await sendToCloudManager(stringMessageWVDRResponse);
                        }
                        else
                        {
                            //Handle error
                            Debug.WriteLine("This token does not exists in database" + nstmessage.Token);
                            await Logging.WriteDebugLog("This token does not exists in database - {0}", nstmessage.Token);
                        }

                        break;


                    case NstMessage.RST_CMG:

                        //When CMG is reset

                        Debug.WriteLine("CMG" + nstmessage.Ipnode + "is reset!!!");
                        await Logging.WriteDebugLog("CMG {0} is reset!!!", nstmessage.Ipnode);

                        await Task.Delay(360000); //wait 6 minutes to connect all CMDS to NST network before Subscription starts

                        await SubscribeCMGNetwork(nstmessage.Ipnode);

                        break;

                    case NstMessage.RST_CMD:

                        //When CMD is reset

                        Debug.WriteLine("CMD" + nstmessage.Ipnode + "is reset!!!");
                        await Logging.WriteDebugLog("CMD {0} is reset!!!", nstmessage.Ipnode);

                        string cmdIpnode = nstmessage.Ipnode;

                        //await SubscribeSpecificCMD(cmdIpnode);

                        //Send subscriptions for the specific CMD xx


                        break;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine("processNstMessage: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("processNstMessage {0} ", ex.Message);
            }
        }

        //private static Task SubscribeCMGNetwork(string ipnode)
        //{
        //    throw new NotImplementedException();
        //}



        //-------------------- Checks if the received TOKEN matches to app_commands table-------------------------------------//
        static private async Task<bool> dbCheckTokenResponse(string token)
        {
            try
            {
                string connectDB = "Filename=c:\\Database\\Gateway.db";
                string selectStatement = "SELECT token FROM app_commands WHERE token = " + "\"" + token.ToUpper() + "\"";

                using (SqliteConnection db = new SqliteConnection(connectDB))
                {
                    db.Open();
                    using (SqliteCommand selectCommand = new SqliteCommand(selectStatement, db))
                    {
                        using (SqliteDataReader rdr = selectCommand.ExecuteReader())
                        {
                            selectCommand.Parameters.AddWithValue("@token", token);
                            try
                            {
                                if (rdr.HasRows)
                                {
                                    db.Close();
                                    return true;
                                }
                                else
                                {
                                    db.Close();
                                    return false;
                                }
                            }
                            catch (SqliteException error)
                            {
                                //Handle error
                                Debug.WriteLine("Problem while executing the sql command:" + error.Message);
                                await Logging.WriteDebugLog("dbCheckTokenResponse - {0} - {1}", "Problem while executing the sql command: ", error.Message);
                                db.Close();
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("dbCheckTokenResponse: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("dbCheckTokenResponse {0} ", ex.Message);
                return false;
            }
        }





        //-------------------- Sends NstMessage to CloudManager---------------------------------------//
        public async static Task sendToCloudManager(object data)
        {
            ValueSet sentData = new ValueSet();
            sentData.Add("Message", data);
            sentData.Add("Sender", "bkNstApplication");
            sentData.Add("Receiver", "bkCloudManager");

            await IPCservice.SendMessageAsync(sentData);
        }


        //--------------------Receives a cloud command---------------------------------------//
        public async static Task processCommandReceived(string receivedCommand)
        {
            try
            {
                NstMessage nstCommand = new NstMessage(receivedCommand);
                await Logging.WriteDebugLog("Received command from cloud or SUB command: {0} ", receivedCommand);

                bool commandSupported = await dbCheckSupportedCommand(nstCommand.Command);

                if (commandSupported == true)
                {
                    // add command to app_commands table
                    //await dbAddReceivedCommand(nstCommand);

                    string xx = await dbAddReceivedCommand(nstCommand);
                    nstCommand.Token = xx;

                    //if (nstCommand.Command.Equals("SUB"))
                    //{
                    //    string ipnode = nstCommand.Ipnode;
                    //        //receivedCommand.Substring(21, 4);

                    //    string cmgUniqueId = await dbGetCMGUniqueID(ipnode)

                    //}

                    // string nstCommandInBytes = await convertNstMessageToBytes(nstCommand);

                    ValueSet sentData = new ValueSet();
                    sentData.Add("Token", nstCommand.Stringify());
                    sentData.Add("Sender", "NstApplication");
                    sentData.Add("Receiver", "bkPortManager");

                    await IPCservice.SendMessageAsync(sentData);
                }
                else
                {
                    Debug.WriteLine("Command is not supported:" + nstCommand.Command);
                    await Logging.WriteDebugLog("Command is not supported: {0}", nstCommand.Command);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("processCommandReceived: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("processCommandReceived: {0}", ex.Message);


            }
        }



        //-------------------------Check if received command is supported----------------------------------------//
        static public async Task<bool> dbCheckSupportedCommand(string receivedCommand)
        {
            try
            {
                string connectDB = "Filename=c:\\Database\\Gateway.db";
                string selectStatement = "SELECT command FROM App_supported_commands WHERE command = " + "\"" + receivedCommand + "\"";

                using (SqliteConnection db = new SqliteConnection(connectDB))
                {
                    db.Open();
                    using (SqliteCommand selectCommand = new SqliteCommand(selectStatement, db))
                    {
                        using (SqliteDataReader rdr = selectCommand.ExecuteReader())
                        {
                            try
                            {
                                if (rdr.HasRows)
                                {
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            catch (SqliteException error)
                            {
                                //Handle error
                                Debug.WriteLine("Problem while executing the sql command:" + error.Message);
                                await Logging.WriteDebugLog("dbCheckSupportedCommand - {0} - {1}", "Problem while executing the sql command: ", error.Message);
                                db.Close();
                                return false;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("dbCheckSupportedCommand: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("dbCheckSupportedCommand {0} ", ex.Message);
                return false;
            }
        }


        //-------------------------Add received command to app_commands table----------------------------------------//
        static public async Task<string> dbAddReceivedCommand(NstMessage commandAdded)
        {
            try
            {
                const string insertStatement = @"INSERT INTO app_commands (ID, commandname, az_message_id) VALUES  (@ID, @commandname, @az_message_id)";

                using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db; Cache=Shared"))
                {
                    db.Open();

                    using (SqliteCommand insertCommand = new SqliteCommand(insertStatement, db))
                    {
                        SqliteCommand selectCommand = null;
                        int result;
                        SqliteDataReader query;
                        string tokenCreated = null;

                        insertCommand.Prepare();
                        insertCommand.Parameters.AddWithValue("@ID", commandAdded.Appid);
                        insertCommand.Parameters.AddWithValue("@commandname", commandAdded.Command);
                        insertCommand.Parameters.AddWithValue("@az_message_id", commandAdded.Azuremessageid);
                        try
                        {
                            //read_uncommited pragma:sets read-uncommitted isolation level to true(1),the connection will not put read locks on the tables it reads. 
                            //Therefore, another writer can actually change a table as the connection in read-uncommitted mode can neither block nor be blocked by any other connections.
                            var pragmaStatement = db.CreateCommand();
                            pragmaStatement.CommandText = "PRAGMA read_uncommitted = 1";
                            pragmaStatement.ExecuteNonQuery();

                            result = insertCommand.ExecuteNonQuery();

                            selectCommand = new SqliteCommand("SELECT token FROM app_commands WHERE app_command_id = last_insert_rowid()", db);
                            query = selectCommand.ExecuteReader();

                            while (query.Read())
                            {
                                tokenCreated = query.GetString(0);
                                Debug.WriteLine("Token:" + tokenCreated);
                            }
                            return tokenCreated;
                            //commandAdded.Token = tokenCreated;
                        }

                        catch (SqliteException error)
                        {
                            //Handle error
                            Debug.WriteLine("Problem while executing the sql command:" + error.Message);
                            await Logging.WriteDebugLog("dbAddReceivedCommand - {0} - {1}", "Problem while executing the sql command: ", error.Message);
                            db.Close();
                            return null;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Debug.WriteLine("dbAddReceivedCommand: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("dbAddReceivedCommand {0} ", ex.Message);
                return null;
            }

        }



        //-----------------Checks whether any CMG is dead (instantiated by HBE command)----------------------// 
        public static async void checkIfCmgIsDead(ThreadPoolTimer timer)
        {
            try
            {
                // if there is at least one token in the dictionary send an ALERT to cloud manager
                //foreach (KeyValuePair<string, string> entry in cmgHBETokens)
                for (int i = 0; i < numOfCMGsDeclaredinDB; i++)
                {
                    bool foundCmg = cmgHBETokens.ContainsKey(CMGsDeclaredinDB[i]);

                    if (foundCmg == true)
                    {
                        Debug.WriteLine("CMG with ipnode: " + CMGsDeclaredinDB[i] + " found DEAD!!! Send ALERT to Cloud");
                        await Logging.WriteDebugLog("CMG[ipnode]: {0} found DEAD!!!", CMGsDeclaredinDB[i]);

                        // CMG is dead! Send an ALERT message to cloud
                        NstMessage alertMessage = new NstMessage();
                        alertMessage.Response = NstMessage.HBE_ALERT_RESPONSE;
                        alertMessage.Ipnode = CMGsDeclaredinDB[i];
                        string stringMessageAlert = alertMessage.Stringify();
                        await sendToCloudManager(stringMessageAlert);

                        //prepare and send a RESET command to CMG
                        //bool commandSupported = await NstApp.dbCheckSupportedCommand(NstMessage.RSG_COMMAND);

                        //if (commandSupported == true)
                        //{

                        //    NstMessage sendRSG = new NstMessage();

                        //    sendRSG.Command = NstMessage.RSG_COMMAND;
                        //    sendRSG.Ipnode = CMGsDeclaredinDB[i];
                        //    Debug.WriteLine("Send RSG command (RESET) to CMG[ipnode]: " + sendRSG.Ipnode);
                        //    await Logging.WriteDebugLog("Send RSG command (RESET) to CMG[ipnode] - {0}", sendRSG.Ipnode);

                        //    string token = await NstApp.dbAddReceivedCommand(sendRSG);
                        //    sendRSG.Token = token;

                        //    // send command to Port Manager
                        //    ValueSet sentData = new ValueSet();
                        //    sentData.Add("Token", sendRSG.Stringify());
                        //    sentData.Add("Sender", "NstApplication");
                        //    sentData.Add("Receiver", "bkPortManager");
                        //    await IPCservice.SendMessageAsync(sentData);


                        //}


                        // remove entry from dictionary
                        cmgHBETokens.Remove(CMGsDeclaredinDB[i]);

                        // }

                        // TBD
                        // delete command from app_commands

                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("checkIfCmgIsDead:" + ex.Message);
                await Logging.WriteSystemLog("checkIfCmgIsDead: {0}", ex.Message);
            }
        }



        //Subscribe NST Network for first time
        static public async Task dbSubcribeNSTNetwork()
        {
            try
            {
                string selectStatement = "SELECT CMG_IPV6_Address from CMGs";
                string[] getCMGipnode = new string[3];

                using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db; Cache=Shared"))
                {
                    db.Open();

                    using (SqliteCommand selectCommand = new SqliteCommand(selectStatement, db))
                    {
                        SqliteDataReader query;

                        //selectCommand.Parameters.AddWithValue("@CMGID", CMGID);

                        try
                        {
                            var pragmaStatement = db.CreateCommand();
                            pragmaStatement.CommandText = "PRAGMA read_uncommitted = 1";
                            pragmaStatement.ExecuteNonQuery();

                            query = selectCommand.ExecuteReader();

                            int i = 0;
                            while (query.Read())
                            {
                                getCMGipnode[i] = query.GetString(0);
                                Debug.WriteLine("getCMGipnode:" + query.GetString(0));

                                await SubscribeCMGNetwork(getCMGipnode[i]);
                            }
                        }
                        catch (SqliteException error)
                        {
                            //Handle error
                            Debug.WriteLine("Problem while executing the sql command:" + error.Message);
                            await Logging.WriteSystemLog("dbSubcribeNSTNetwork - {0} Problem while executing the sql command: ", error.Message);
                        }
                        db.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("dbSubcribeNSTNetwork:" + ex.Message);
                await Logging.WriteSystemLog("dbSubcribeNSTNetwork: {0}", ex.Message);
            }
        }


        static public async Task SubscribeCMGNetwork(string CMGipnode)
        {
            try
            {
                string selectStatement = "SELECT CMD_IPV6_Address from CMDs where CMGID in (select CMGID from CMGs where CMG_IPV6_Address = @CMGipnode)";
                string CMD_IPV6_Address = null;
                string DVC_Resource_type = null;

                using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db; Cache=Shared"))
                {
                    db.Open();

                    using (SqliteCommand selectCommandCMG = new SqliteCommand(selectStatement, db))
                    {
                        SqliteCommand selectCommandCMD = null;
                        SqliteDataReader queryCMG;
                        SqliteDataReader queryCMD;

                        selectCommandCMG.Parameters.AddWithValue("@CMGipnode", CMGipnode);
                        try
                        {
                            var pragmaStatement = db.CreateCommand();
                            pragmaStatement.CommandText = "PRAGMA read_uncommitted = 1";
                            pragmaStatement.ExecuteNonQuery();

                            queryCMG = selectCommandCMG.ExecuteReader();

                            while (queryCMG.Read())
                            {
                                CMD_IPV6_Address = queryCMG.GetString(0);
                                Debug.WriteLine("CMD_IPV6_Address:" + queryCMG.GetString(0));
                                selectCommandCMD = new SqliteCommand("SELECT DVC_Resource_type from DEVICES where CMDID in (select CMDID from CMDs where CMD_IPV6_Address = @CMDipnode)", db);
                                selectCommandCMD.Parameters.AddWithValue("@CMDipnode", CMD_IPV6_Address);

                                pragmaStatement.CommandText = "PRAGMA read_uncommitted = 1";
                                pragmaStatement.ExecuteNonQuery();

                                queryCMD = selectCommandCMD.ExecuteReader();
                                while (queryCMD.Read())
                                {
                                    DVC_Resource_type = queryCMD.GetString(0);
                                    Debug.WriteLine("DVC_Resource_type:" + queryCMD.GetString(0));

                                    NstMessage SubCommand = new NstMessage();
                                    SubCommand.Command = "SUB";
                                    SubCommand.Ipnode = CMD_IPV6_Address;
                                    SubCommand.Resource = DVC_Resource_type;

                                    await processCommandReceived(SubCommand.Stringify());
                                }
                            }
                        }
                        catch (SqliteException error)
                        {
                            //Handle error
                            Debug.WriteLine("Problem while executing the sql command:" + error.Message);
                            await Logging.WriteSystemLog("SubscribeCMGNetwork - {0} Problem while executing the sql command: ", error.Message);
                            //db.Close();
                            return;
                        }
                        db.Close();

                    }
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine("SubscribeCMGNetwork:" + ex.Message);
                await Logging.WriteSystemLog("SubscribeCMGNetwork: {0}", ex.Message);
            }

        }

        //static public async Task SubscribeSpecificCMD(string CMDipnode)
        //{
        //    try
        //    {
        //        string selectStatement = "SELECT CMD_IPV6_Address from CMDs where CMD_IPV6_Address = @CMDipnode";
        //        string CMD_IPV6_Address = null;
        //        using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db; Cache=Shared"))
        //        {
        //            db.Open();
        //            using (SqliteCommand selectCommandCMG = new SqliteCommand(selectStatement, db))
        //            {
        //                SqliteCommand selectCommandCMD = null;
        //                SqliteDataReader queryCMG;
        //                SqliteDataReader queryCMD;

        //                selectCommandCMD.Parameters.AddWithValue("@CMDipnode", CMDipnode);
        //                try
        //                {
        //                    var pragmaStatement = db.CreateCommand();
        //                    pragmaStatement.CommandText = "PRAGMA read_uncommitted = 1";
        //                    pragmaStatement.ExecuteNonQuery();

        //                    queryCMG = selectCommandCMG.ExecuteReader();

        //                    while (queryCMG.Read())
        //                    {
        //                        CMD_IPV6_Address = queryCMG.GetString(0);
        //                        Debug.WriteLine("CMD_IPV6_Address:" + queryCMG.GetString(0));
        //                        selectCommandCMD = new SqliteCommand("SELECT DVC_Resource_type from DEVICES where CMDID in (select CMDID from CMDs where CMD_IPV6_Address = @CMDipnode)", db);
        //                        selectCommandCMD.Parameters.AddWithValue("@CMDipnode", CMD_IPV6_Address);

        //                        pragmaStatement.CommandText = "PRAGMA read_uncommitted = 1";
        //                        pragmaStatement.ExecuteNonQuery();

        //                        queryCMD = selectCommandCMD.ExecuteReader();
        //                        while (queryCMD.Read())
        //                        {
        //                            DVC_Resource_type = queryCMD.GetString(0);
        //                            Debug.WriteLine("DVC_Resource_type:" + queryCMD.GetString(0));

        //                            NstMessage SubCommand = new NstMessage();
        //                            SubCommand.Command = "SUB";
        //                            SubCommand.Ipnode = CMD_IPV6_Address;
        //                            SubCommand.Resource = DVC_Resource_type;

        //                            await processCommandReceived(SubCommand.Stringify());
        //                        }
        //                    }
        //                }
        //                catch (SqliteException error)
        //                {
        //                    //Handle error
        //                    Debug.WriteLine("Problem while executing the sql command:" + error.Message);
        //                    await Logging.WriteSystemLog("SubscribeCMGNetwork - {0} Problem while executing the sql command: ", error.Message);
        //                    //db.Close();
        //                    return;
        //                }
        //                db.Close();

        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine("SubscribeSpecificCMD:" + ex.Message);
        //        await Logging.WriteSystemLog("SubscribeSpecificCMD: {0}", ex.Message);
        //    }
        //}
    }
}





     












