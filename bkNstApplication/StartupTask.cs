﻿using System;
using Windows.ApplicationModel.Background;
using System.Threading.Tasks;
using Windows.Storage;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Windows.ApplicationModel.AppService;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.System.Threading;
using Microsoft.Data.Sqlite;
using comNST;
using System.Collections.Generic;

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace bkNstApplication
{

    public sealed class StartupTask : IBackgroundTask
    {

        private ThreadPoolTimer _timer;
        private BackgroundTaskDeferral _deferral;


        //int count = 0;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            // Get a BackgroundTaskDeferral and hold it forever if initialization is sucessful.
            _deferral = taskInstance.GetDeferral();

            //AppServiceBridge.RequestReceived += AppServiceRequestHandler;
            await IPCservice.InitAsync();

            taskInstance.Canceled += (IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason) =>
            {
                Debug.WriteLine("Cancelled: reason " + reason);
                _deferral.Complete();
            };

            MemoryManager.AppMemoryUsageIncreased += MemoryManager_AppMemoryUsageIncreased;

            Debug.WriteLine("NstApplication just started!");
            await Logging.WriteDebugLog("NstApplication just started! - {0}");

            _timer = ThreadPoolTimer.CreatePeriodicTimer(dbCleanUp_AppCommands, TimeSpan.FromMinutes(5));
            //dbCleanUp_AppCommands(null);



            // Get all valid CMGs and keep their ipnode in a dictionary
            NstApp.CMGsDeclaredinDB = await dbGetAllCMGs();

            //Subscribe NST Network
            await Task.Delay(360000); //wait 6  minutes to connect all CMDS to NST network before Subscription starts
            await NstApp.dbSubcribeNSTNetwork();


            // Set a timer to send HBE commands to all valid CMGs
            _timer = ThreadPoolTimer.CreatePeriodicTimer(sendHeartBeatCommandToCMGs, TimeSpan.FromMinutes(10));
            //sendHeartBeatCommandToCMGs(null);

        }

        private void MemoryManager_AppMemoryUsageIncreased(object sender, object e)
        {
            var level = MemoryManager.AppMemoryUsageLevel;
            if (level != AppMemoryUsageLevel.Low)
            {
                Debug.WriteLine($"Memory limit {MemoryManager.AppMemoryUsageLevel} crossed: Current: {MemoryManager.AppMemoryUsage}, limit: {MemoryManager.AppMemoryUsageLimit}");
            }
        }




        // App_Command table: Clean up all commands with waiting_time>300 seconds 
        private async void dbCleanUp_AppCommands(ThreadPoolTimer timer)
        {

            using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db; Cache=Shared"))
            {
                SqliteCommand deletecmd = null;
                db.Open();

                deletecmd = db.CreateCommand();

                deletecmd.CommandText = "DELETE from app_commands where strftime('%s', 'now') - strftime('%s', timeCreated) > @minutes";
                deletecmd.Prepare();
                deletecmd.Parameters.AddWithValue("@minutes", NstMessage.WAITING_TIME);

                try
                {
                    var pragmaStatement = db.CreateCommand();
                    pragmaStatement.CommandText = "PRAGMA read_uncommitted = 1";
                    pragmaStatement.ExecuteNonQuery();

                    deletecmd.ExecuteNonQuery();
                }
                catch (SqliteException error)
                {

                    //Handle error
                    Debug.WriteLine("dbCleanUp_AppCommands - while deleting commands from app_commands table" + error.Message);
                    await Logging.WriteDebugLog(" dbCleanUp_AppCommands - while deleting commands from app_commands table- {0}", error.Message);
                }
                db.Close();


            }
        }

        private async void sendHeartBeatCommandToCMGs(ThreadPoolTimer timer)
        {
            try
            {

                // For each CMG declared in DB
                //  - send a HBE command
                //  - keep in an array the token created in DB
                //  - set a new timer to check whether a response has not arrived - send ALERT to Cloud
                // Check whether the HBE command is supported by current Gateway
                bool commandSupported = await NstApp.dbCheckSupportedCommand(NstMessage.HBE_COMMAND);

                if (commandSupported == true)
                {
                    for (int i = 0; i < NstApp.numOfCMGsDeclaredinDB; i++)
                    {
                        // add command to app_commands table
                        NstMessage nstCommand = new NstMessage();
                        nstCommand.Command = NstMessage.HBE_COMMAND;
                        nstCommand.Ipnode = NstApp.CMGsDeclaredinDB[i];
                        Debug.WriteLine("Send HBE to CMG[ipnode]: " + nstCommand.Ipnode);
                        await Logging.WriteDebugLog("Send HBE to CMG[ipnode] - {0}", nstCommand.Ipnode);
                        string token = await NstApp.dbAddReceivedCommand(nstCommand);
                        nstCommand.Token = token;

                        // update dictionary with the derived token
                        NstApp.cmgHBETokens.Add(NstApp.CMGsDeclaredinDB[i], token);

                        // send command to Port Manager
                        ValueSet sentData = new ValueSet();
                        sentData.Add("Token", nstCommand.Stringify());
                        sentData.Add("Sender", "NstApplication");
                        sentData.Add("Receiver", "bkPortManager");
                        await IPCservice.SendMessageAsync(sentData);


                    }
                    _timer = ThreadPoolTimer.CreateTimer(NstApp.checkIfCmgIsDead, TimeSpan.FromMinutes(1));
                }
                else
                {
                    Debug.WriteLine("HBE Command is not supported");
                    await Logging.WriteDebugLog("HBE Command is not supported");

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("sendHeartBeatCommandToCMGs: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("sendHeartBeatCommandToCMGs {0} ", ex.Message);
            }
        }

        private static async Task<string[]> dbGetAllCMGs()
        {
            try
            {
                using (SqliteConnection db = new SqliteConnection("Filename=c:\\Database\\Gateway.db"))
                {
                    SqliteCommand selectCommand;
                    SqliteDataReader query;
                    string[] connectedCMGs = new string[8];

                    try
                    {
                        db.Open();

                        // ----------  Read CMG table -----------------
                        selectCommand = new SqliteCommand("SELECT CMG_IPV6_Address from CMGs", db);
                        query = selectCommand.ExecuteReader();

                    }
                    catch (SqliteException error)
                    {
                        //Handle error
                        Debug.WriteLine("Problem while executing the sql command:" + error.Message);
                        await Logging.WriteDebugLog("dbGetAllCMGs - {0} - {1}", "Problem while executing the sql command: ", error.Message);
                        return null;
                    }

                    int i = 0;
                    while (query.Read())
                    {
                        //entries.Add("   CMG Id: " + query.GetString(0) + " GatewayID: " + query.GetString(1));
                        connectedCMGs[i] = query.GetString(0);
                        i++;
                    }
                    NstApp.numOfCMGsDeclaredinDB = i;

                    db.Close();
                    return connectedCMGs;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine("sendHeartBeatCommandToCMGs: Exception thrown: " + ex.Message);
                await Logging.WriteSystemLog("sendHeartBeatCommandToCMGs {0} ", ex.Message);
                return null;
            }
        }
    }
}


